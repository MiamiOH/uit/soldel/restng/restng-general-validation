# General Validation RESTful Web Service

## Description

This repository contains the source code for the Validation web service.  This web service returns all general validation table records.

restng-general-validatoin has been converted into RESTng 2.0 requirements. RESTng 2.0 conversion task focued on fixing syntax or directory structure to meet RESTng 2.0. No functionality or logic change were made.  After conversion was done, PHPUnit has been executed.

## Database

The tables used by this application are set in CAM.  Search for GeneralValidation and click on the Validation entry Configuration Items to see what tables are used.  All queries used by this web service are `SELECT`s.

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit. 

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.

## Documentation Listing

* [Asset Information](https://miamioh.teamdynamix.com/TDNext/Apps/741/Assets/AssetDet?AssetID=430457)
* [SwaggerUI (Dev)](https://wsdev.apps.miamioh.edu/api/swagger-ui/#!/Validation/get_validation_v1_tableName)

## More API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/validation