<?php

namespace MiamiOH\RestngGeneralValidation\Tests\Unit;

/*
-----------------------------------------------------------
FILE NAME: getValidationTest.php

Copyright (c) 2018 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Meenakshi Kandasamy

DESCRIPTION: 


ENVIRONMENT DEPENDENCIES: 
RESTng Framework
PHPUnit

TABLE USAGE:

Web Service Usage:

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

03/12/2018              Kandasm
Description:            Initial Draft
			 
-----------------------------------------------------------
*/

class GetValidationTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $request, $dbh, $general, $configTableName;

    // set up method automatically called by PHPUnit before every test method:
    protected function setUp()
    {
        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->general = new \MiamiOH\RestngGeneralValidation\Services\Validation();

        $this->general->setDatabase($db);
        $this->general->setLogger();
        $this->general->setDatasource($ds);
        $this->general->setRequest($this->request);

        $this->configTableName = Array
        (
            '0' => 'ftvacci',
            '1' => 'ftvacct',
            '2' => 'ftvatyp',
            '3' => 'stvrelt'
        );
        $this->general->setConfigTableNames($this->configTableName);
    }

    public function testGetValidationDataWithFilterOptions()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsColumnToSelect')));

        $this->request->method('getResourceParam')
            ->with('tableName')
            ->willReturn('stvrelt');

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryForTheSelectedColumns')));

        $response = $this->general->getValidationData();

        $this->assertEquals($this->mockExpectedValidationResponseForSelectedColumns(), $response->getPayload());

    }

    public function mockOptionsColumnToSelect()
    {
        return array('columnToSelect' => array
        (
            '0' => 'stvrelt_code',
            '1' => 'stvrelt_desc',
            '2' => 'stvrelt_user_id'),

            'renameDBColumnName' => array('0' => 'relationshipCode',
                '1' => 'relationshipDescription',
                '2' => 'relationshipUserId'
            ));
    }

    public function mockQueryForTheSelectedColumns()
    {
        return array(
            array(
                'stvrelt_code' => 'A',
                'stvrelt_desc' => 'Sister',
                'stvrelt_user_id' => 'testing1'
            ),
            array(
                'stvrelt_code' => 'B',
                'stvrelt_desc' => 'Brother',
                'stvrelt_user_id' => 'testing2'
            ),
            array(
                'stvrelt_code' => 'C',
                'stvrelt_desc' => 'Child',
                'stvrelt_user_id' => 'testing3'
            ),
        );
    }

    public function mockExpectedValidationResponseForSelectedColumns()
    {
        $expectedReturn =
            array(
                array(
                    'relationshipCode' => 'A',
                    'relationshipDescription' => 'Sister',
                    'relationshipUserId' => 'testing1'
                ),
                array(
                    'relationshipCode' => 'B',
                    'relationshipDescription' => 'Brother',
                    'relationshipUserId' => 'testing2'
                ),
                array(
                    'relationshipCode' => 'C',
                    'relationshipDescription' => 'Child',
                    'relationshipUserId' => 'testing3'
                ),
            );

        return $expectedReturn;
    }

    public function testNoFilter()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockNoParameters')));

        $this->request->method('getResourceParam')
            ->with('tableName')
            ->willReturn('stvrelt');

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryAllValidations')));

        $response = $this->general->getValidationData();

        $this->assertEquals($this->mockExpectedAllValidationResponse(), $response->getPayload());

    }

    public function mockExpectedAllValidationResponse()
    {
        $expectedReturn =
            array(
                array(
                    'code' => 'A',
                    'description' => 'Sister',
                ),
                array(
                    'code' => 'B',
                    'description' => 'Brother',
                ),
                array(
                    'code' => 'C',
                    'description' => 'Child',
                ),
            );

        return $expectedReturn;
    }

    public function mockQueryAllValidations()
    {
        return array(
            array(
                'stvrelt_code' => 'A',
                'stvrelt_desc' => 'Sister',
            ),
            array(
                'stvrelt_code' => 'B',
                'stvrelt_desc' => 'Brother',
            ),
            array(
                'stvrelt_code' => 'C',
                'stvrelt_desc' => 'Child',
            ),
        );
    }

    public function mockNoParameters()
    {
        $optionsArray = array();
        return $optionsArray;
    }

    public function testValidationDataWithValidParam()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsValidationFilter')));

        $this->request->method('getResourceParam')
            ->with('tableName')
            ->willReturn('stvrelt');

        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryValidationFilter')));

        $response = $this->general->getValidationData();
        $this->assertEquals($this->mockExpectedValidationFilterResults(), $response->getPayload());
    }

    public function mockOptionsValidationFilter()
    {
        return array(
            'generalCode' => 'A'
        );
    }

    public function mockQueryValidationFilter()
    {
        return array(
            array(
                'stvrelt_code' => 'A',
                'stvrelt_desc' => 'Sister',
            )
        );
    }

    public function mockExpectedValidationFilterResults()
    {
        $expectedReturn =
            array(
                array(
                    'code' => 'A',
                    'description' => 'Sister',
                )
            );
        return $expectedReturn;
    }

    public function testInvalidTableNameAsParameter()
    {
        $this->request->method('getResourceParam')
            ->with('tableName')
            ->willReturn('invalidTableName');

        try {
            $response = $this->general->getValidationData();

            $this->fail();
        } catch (\Exception $e) {
            $this->assertEquals('Error: table is not configured in config manager', $e->getMessage());
        }
    }

}