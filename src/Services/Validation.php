<?php

/*
	-----------------------------------------------------------
	FILE NAME: Validation.class.php

	Copyright (c) 2018 Miami University, All Rights Reserved.

	Miami University grants you ("Licensee") a non-exclusive, royalty free,
	license to use, modify and redistribute this software in source and
	binary code form, provided that i) this copyright notice and license
	appear on all copies of the software; and ii) Licensee does not utilize
	the software in a manner which is disparaging to Miami University.

	This software is provided "AS IS" and any express or implied warranties,
	including, but not limited to, the implied warranties of merchantability
	and fitness for a particular purpose are disclaimed. It has been tested
	and is believed to work as intended within Miami University's
	environment. Miami University does not warrant this software to work as
	designed in any other environment.

	AUTHOR: Meenakshi Kandasamy

	DESCRIPTION:  The general validation service is currently designed for GET only to get validation code and description

	INPUT:
	PARAMETERS:

	ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

	TABLE USAGE:

	AUDIT TRAIL:

	DATE    PRJ-TSK          UniqueID
	Description:

	05/15/2018		        kandasm
	Description:	        Initial Program
 */

namespace MiamiOH\RestngGeneralValidation\Services;

class Validation extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $database = '';
    private $configuration;
    private $datasource_name = 'MUWS_GEN_PROD'; // secure datasource

    private $config = array();
    private $configTableName;


    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the datasource object provided by the framework
    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    // Inject the configuration object provided by the framework
    public function setConfiguration($configuration)
    {
        $this->config = $configuration->getConfiguration('GeneralValidation', 'Validation');
        $tables = isset($this->config['validationTableName']) ? $this->config['validationTableName'] : '';
        $this->configTableName = explode(",", $tables);
    }

    public function setConfigTableNames($tableString)
    {
        return $this->configTableName = $tableString;
    }

    public function getValidationData()
    {
        $this->log->debug('getting data');

        //set up some variables for use later
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        $tableName = '';
        $generalCode = '';
        $columnToSelect = '';
        $renameDBColumnName = '';

        if (in_array($request->getResourceParam('tableName'), $this->configTableName)) {
            if (preg_match('/^[a-z]tv[A-Za-z0-9]{4}$/', $request->getResourceParam('tableName'))) {
                $tableName = $request->getResourceParam('tableName');
            } else {
                throw new \Exception('Error: table name is invalid');
            }
        } else {
            throw new \Exception('Error: table is not configured in config manager');
        }

        if (isset($options['generalCode'])) {
            $generalCode = strtoupper(trim($options['generalCode']));
        }

        if (isset($options['columnToSelect'])) {
            $columnToSelect = implode(',', array_map('strtolower', $options['columnToSelect']));
        }
        if (isset($options['renameDBColumnName'])) {
            $renameDBColumnName = implode(',', $options['renameDBColumnName']);
        }

        if ($tableName === null) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        if (isset($options['columnToSelect'])) {
            $queryString = "select $columnToSelect from " . $tableName;
        } else {
            $queryString = "select distinct * from " . $tableName;
        }

        if ($generalCode) {
            $queryString .= ' where ' . $tableName . '_code = ?';
        }

        $dbh = $this->database->getHandle($this->datasource_name);


        if ($generalCode) {
            $results = $dbh->queryall_array($queryString, $generalCode);
        } else {
            $results = $dbh->queryall_array($queryString);
        }

        if ($columnToSelect) {
            $payload = $this->buildGeneral($results, $tableName, $columnToSelect, $renameDBColumnName);
        } else {
            $payload = $this->buildGeneral($results, $tableName);
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    private function buildGeneral($dbResults, $tableName, $columnToSelect = null, $renameDBColumnName = null)
    {
        $formattedGeneralRecords = array();

        if (!empty($columnToSelect)) {
            $columnToSelect = explode(",", $columnToSelect);
            if (!empty($renameDBColumnName)) {
                $renameDBColumnName = explode(",", $renameDBColumnName);
                foreach ($dbResults as $record) {
                    $tmp = array();
                    for ($x = 0; $x < count($columnToSelect); $x++) {
                        $tmp[$renameDBColumnName[$x]] = $record[$columnToSelect[$x]];
                    }
                    $formattedGeneralRecords[] = $tmp;
                }
            } else {
                foreach ($dbResults as $record) {
                    $tmp = array();
                    foreach ($columnToSelect as $columnName) {
                        $tmp[$columnName] = $record[$columnName];
                    }
                    $formattedGeneralRecords[] = $tmp;
                }
            }

        } else {
            foreach ($dbResults as $record) {
                $tmp = array();
                $tmp['code'] = $record[$tableName . '_code'];
                $tmp['description'] = $record[$tableName . '_desc'];
                $formattedGeneralRecords[] = $tmp;
            }
        }
        return $formattedGeneralRecords;
    }
}
