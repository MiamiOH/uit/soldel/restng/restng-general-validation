<?php

namespace MiamiOH\RestngGeneralValidation\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ValidationResourceProvider extends ResourceProvider
{

    private $tag = "";
    private $dot_path = "Validation";
    private $s_path = "/validation/v1";
    private $bs_path = '\Validation';

    public function registerDefinitions(): void
    {
//        $this->addTag(array(
//   'name' => 'IA',
//    'description' => ''
//));


            $this->addDefinition(array(
                'name' => $this->dot_path . '.Get.Return.Validation',
                'type' => 'object',
                'properties' => array(
                    'code' => array('type' => 'string', 'description' => 'Validation code'),
                    'description' => array('type' => 'string', 'description' => 'Description/name'),
                )
            ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Validation.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Validation',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Validation',
            'class' => 'MiamiOH\RestngGeneralValidation\Services' . $this->bs_path,
            'description' => 'This service provides resources for general validation tables',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => $this->dot_path . '.get',
                'description' => 'Return all general validation table records',
                'pattern' => $this->s_path . '/' . ':tableName',
                'service' => 'Validation',
                'method' => 'getValidationData',
                'isPageable' => false,
                //'tags' => array($tag),
                'returnType' => 'collection',
                'params' => array(
                    'tableName' => array('description' => 'Name of the Validation table'),
                ),
                'options' => array(
                    'columnToSelect' => array('type' => 'list', 'description' => 'list the column name that needs to select from the table Ex(stvterm_code,stvterm_Desc)'),
                    'renameDBColumnName' => array('type' => 'list', 'description' => 'Rename the column name to be nice one Ex(termCode,termDescription)'),
                    'generalCode' => array('type' => 'single', 'description' => 'validationCode'),
                ),

                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Validation validation table, their codes and description',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Validation.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}